# -*- coding: utf-8 -*-

import json
import csv 

with open ("conflict_data_full_lined.json") as my_file:
    data = json.load(my_file)

#
#for conflict in data:
#      if conflict["country"] == "Sudan" or conflict["country"] == "South Sudan":
#        if conflict["year"] in range(2009,2014):  #from 2009 up to but not includuing 2014 (so 2013)
#            print([conflict["country"], conflict["year"], conflict["best"]])





#Open the output CSV file we want to write to
with open('preprocessed_data2.csv', 'w', newline='') as file:
    csvwriter = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    
    csvwriter.writerow(['Country', 'Year', 'Deaths'])
    # Write 3 rows after each other, containing e.g. [2009, "Sudan", 203], [2015,...]
    for conflict in data:
      if conflict["country"] == "Sudan" or conflict["country"] == "South Sudan":
         if conflict["year"] in range(2009,2014):
            csvwriter.writerow([conflict["country"], conflict["year"], conflict["best"]])
#
#print("Done writing CSV file")    
    
    # Actually write the data to the CSV file here.
    # You can use the same csvwriter.writerow command to output the data 
    #   as is used above to output the headers.